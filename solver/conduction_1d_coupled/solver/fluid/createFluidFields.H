// Initialise fluid field pointer lists
PtrList<volScalarField> tempFluid(fluidRegions.size());
PtrList<IOdictionary> fluidTransportProperties(fluidRegions.size());

// Populate fluid field pointer lists
forAll(fluidRegions, i)
{
    Info<< "*** Reading fluid mesh thermophysical properties for region "
        << fluidRegions[i].name() << nl << endl;

    Info<< "    Adding to tempFluid\n" << endl;
    tempFluid.set
    (
        i,
        new volScalarField
        (
            IOobject
            (
                "T",
                runTime.timeName(),
                fluidRegions[i],
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE
            ),
            fluidRegions[i]
        )
    );

    fluidTransportProperties.set
    (
	i,
	new IOdictionary 
	(
		IOobject
		(
		  "transportProperties",
		  runTime.constant(),
		  fluidRegions[i],
		  IOobject::MUST_READ_IF_MODIFIED,
		  IOobject::NO_WRITE
		)
	)

    );
}
