
forAll(fluidRegions, i)
{
    // Note: do not use setRegionSolidFields.H to avoid double registering Cp
    //#include "setRegionSolidFields.H"
    const IOdictionary& transportProperties= fluidTransportProperties[i];
    dimensionedScalar kappa("kappa",dimViscosity,transportProperties.lookup("kappa"));
    
    DiNum = max
    (
        solidRegionDiffNo
        (
            fluidRegions[i],
            runTime,
            kappa
        ),
        DiNum
    );

}
